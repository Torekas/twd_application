import dash
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
from dash import dcc, html
import time

radar_plot = go.Figure()
global button_id
app = dash.Dash(__name__)


def get_season(date):
    if date.month in [3, 4, 5]:
        return 'Spring'
    elif date.month in [6, 7, 8]:
        return 'Summer'
    elif date.month in [9, 10, 11]:
        return 'Fall'
    else:
        return 'Winter'


app.layout = html.Div(
    style={"font-family": "Georgia", "background-color": "#F2E9DD"},
    children=[
        html.Div(
            [
                html.H1(
                    "Choose One Of Us",
                    style={"text-align": "center", 'padding-top': '2.5%'},
                ),
                html.Button(
                    id='buttonBogusia',
                    children='Bogusia',
                    n_clicks=0,
                    style={'background-color': "#1DB954", 'font-size': '16px', 'margin': '10px',
                           'border-radius': '5px', 'font-size': '28px', 'padding': '16px 40px',
                           'text-shadow': '0px 1px 0px #2f6627', 'font-family': 'Georgia', 'color': 'white',
                           'width': '15%'}
                ),

                html.Button(
                    id='buttonWiktoria',
                    children='Wiktoria',
                    n_clicks=0,
                    style={'background-color': "#1DB954", 'font-size': '16px', 'margin': '10px',
                           'border-radius': '5px', 'font-size': '28px', 'padding': '16px 40px',
                           'text-shadow': '0px 1px 0px #2f6627', 'font-family': 'Georgia', 'color': 'white',
                           'width': '15%'}
                ),
                html.Button(
                    id='buttonKasia',
                    children='Kasia',
                    n_clicks=0,
                    style={'background-color': "#1DB954", 'font-size': '16px', 'margin': '10px',
                           'border-radius': '5px', 'font-size': '28px', 'padding': '16px 40px',
                           'text-shadow': '0px 1px 0px #2f6627', 'font-family': 'Georgia', 'color': 'white',
                           'width': '15%'}
                ),
            ],
            style={'text-align': 'center'}),
        dcc.Tabs(
            [
                dcc.Tab(
                    label="Most Listend Artists And Albums",
                    children=[
                        html.Div(
                            [
                                html.H1(
                                    "Top 10 most listened artists in this semester",
                                    style={"text-align": "center"}),
                                html.Div(
                                    [

                                    ],
                                    className="row",
                                    id='div_1_content',
                                    style={'text-align': 'center'}
                                )
                            ]
                        ),
                        html.Div(
                            [
                                html.H1(
                                    "Top 10 most listened albums in this semester",
                                    style={"text-align": "center"}
                                ),
                                html.Div(
                                    [

                                    ],
                                    className="row",
                                    id='div_2_content',
                                    style={'text-align': 'center'}
                                )
                            ]
                        ),
                        html.Div(
                            style={
                                "height": "100px",
                                "background-color": "#FA8128",
                                "margin-top": "20px",
                            }
                        ),
                    ], style={"background-color": "#FA8128"},
                ),
                dcc.Tab(
                    label="Productivity VS Music Preferences",
                    children=[
                        html.Div(
                            [

                            ],
                            id='div_3_content'
                        ),
                        html.Div(
                            [
                                html.H2(
                                    "Music genre/artists/songs(and features) preferences depending on Productivity",
                                    style={"text-align": "center"},
                                ),
                                html.Div(
                                    [
                                        html.Div(
                                            [
                                                html.Label(
                                                    "Choose Productivity Level:",
                                                    style={"font-size": "20px"},
                                                ),
                                                dcc.Checklist(
                                                    id="checkboxes1",
                                                    options=[
                                                        {
                                                            "label": "High",
                                                            "value": "High",
                                                        },
                                                        {
                                                            "label": "Medium",
                                                            "value": "Medium",
                                                        },
                                                        {
                                                            "label": "Low",
                                                            "value": "Low",
                                                        },
                                                    ],
                                                    value=["High", "Medium", "Low"],
                                                    style={"font-size": "20px"},
                                                ),
                                            ],
                                            style={
                                                "width": "20%",
                                                "float": "left",
                                                "margin": "1.5%",
                                            },
                                        ),
                                        html.Div(
                                            [
                                                html.Label(
                                                    "Choose Grouping:",
                                                    style={"font-size": "20px"},
                                                ),
                                                dcc.Dropdown(
                                                    id="grouping-dropdown",
                                                    options=[
                                                        {
                                                            "label": "Artists",
                                                            "value": "master_metadata_album_artist_name",
                                                        },
                                                        {
                                                            "label": "Track Name",
                                                            "value": "master_metadata_track_name",
                                                        },
                                                        {
                                                            "label": "Track Genre",
                                                            "value": "track_genre",
                                                        },
                                                        {
                                                            "label": "Album",
                                                            "value": "master_metadata_album_album_name",
                                                        },
                                                    ],
                                                    value="track_genre",
                                                    style={
                                                        "font-size": "20px",
                                                    },
                                                ),
                                            ],
                                            style={
                                                "width": "30%",
                                                "float": "right",
                                            },
                                        ),
                                    ],
                                    style={"display": "flex"},
                                ),
                                html.Div(
                                    dcc.Graph(id="genre_preference"),
                                    style={
                                        "width": "97%",
                                        "box-shadow": "2px 2px 2px 2px rgba(0.3, 0.3, 0.3, 0.3)",
                                        "margin": "auto",
                                        "background-color": "#E8DAC5",
                                    },
                                ),
                                html.Div(
                                    [
                                        html.Div(
                                            dcc.Graph(
                                                id="radar_plot",
                                                figure=radar_plot,
                                            ),
                                            style={
                                                "width": "47%",
                                                "box-shadow": "2px 2px 2px 2px rgba(0.3, 0.3, 0.3, 0.3)",
                                                "margin": "1.5%",
                                                "background-color": "#E8DAC5",
                                            },
                                        ),
                                        html.Div(
                                            [
                                                html.H2(
                                                    "How to read this radar plot?",
                                                    style={"text-align": "center"},
                                                ),
                                                dcc.Tabs(
                                                    [
                                                        dcc.Tab(label='acousticness', children=[
                                                            # Content for Tab 1
                                                            html.H3(
                                                                "A confidence measure from 0.0 to 1.0 of whether the track is acoustic. 1.0 represents high confidence the track is acoustic.",
                                                                style={"text-align": "left", 'margin': '10px'}),
                                                        ], style={"background-color": "#FA8128"}),
                                                        dcc.Tab(label='danceability', children=[
                                                            # Content for Tab 2
                                                            html.H3(
                                                                "Danceability describes how suitable a track is for dancing based on a combination of musical elements including tempo, rhythm stability, beat strength, and overall regularity. A value of 0.0 is least danceable and 1.0 is most danceable.",
                                                                style={"text-align": "left", 'margin': '10px'}),
                                                        ], style={"background-color": "#FA8128"}),
                                                        dcc.Tab(label='energy', children=[
                                                            html.H3(
                                                                "Energy is a measure from 0.0 to 1.0 and represents a perceptual measure of intensity and activity. Typically, energetic tracks feel fast, loud, and noisy. For example, death metal has high energy, while a Bach prelude scores low on the scale. Perceptual features contributing to this attribute include dynamic range, perceived loudness, timbre, onset rate, and general entropy.",
                                                                style={"text-align": "left", 'margin': '10px'}),
                                                        ], style={"background-color": "#FA8128"}),
                                                        dcc.Tab(label='loudness', children=[
                                                            html.H3(
                                                                "Ranges from 0 (least loud) to 1 (loudest). Loudness values are averaged across the entire track and are useful for comparing relative loudness of tracks. Loudness is the quality of a sound that is the primary psychological correlate of physical strength (amplitude).",
                                                                style={"text-align": "left", 'margin': '10px'}),
                                                        ], style={"background-color": "#FA8128"}),
                                                    ],
                                                    style={"width": "100%"},
                                                ),
                                                dcc.Tabs(
                                                    [
                                                        dcc.Tab(label='speechiness', children=[
                                                            html.H3(
                                                                "Speechiness detects the presence of spoken words in a track. Ranges from 0 (least speechiness) to 1 (most speechiness).",
                                                                style={"text-align": "left", 'margin': '10px'}),
                                                        ], style={"background-color": "#FA8128"}),
                                                        dcc.Tab(label='valence', children=[
                                                            html.H3(
                                                                "A confidence measure from 0.0 to 1.0 of whether the track is acoustic. 1.0 represents high confidence the track is acoustic.",
                                                                style={"text-align": "left", 'margin': '10px'}),

                                                        ], style={"background-color": "#FA8128"}),
                                                        dcc.Tab(label='liveness', children=[
                                                            html.H3(
                                                                "Detects the presence of an audience in the recording. Higher liveness values represent an increased probability that the track was performed live. A value above 0.8 provides strong likelihood that the track is live.",
                                                                style={"text-align": "left", 'margin': '10px'}),

                                                        ], style={"background-color": "#FA8128"}),
                                                    ],
                                                    style={"width": "100%"},
                                                ),
                                            ],
                                            style={
                                                "width": "47%",
                                                "box-shadow": "2px 2px 2px 2px rgba(0.3, 0.3, 0.3, 0.3)",
                                                "margin": "1.5%",
                                                "background-color": "#E8DAC5",
                                            },
                                        ),
                                    ],
                                    style={"display": "flex"},
                                )
                            ]
                        ),
                        html.Div(
                            style={
                                "height": "100px",
                                "background-color": "#FA8128",
                                "margin-top": "20px",
                            }
                        ),
                    ], style={"background-color": "#FA8128"},
                ),
                dcc.Tab(
                    label="Date",
                    children=[

                        html.Div(
                            [
                                html.H1(
                                    "The total amount of time spent on Spotify vs different time periods",
                                    style={"text-align": "center"},
                                ),
                                html.Div(
                                    [
                                        html.Div(
                                            [
                                                html.Label(
                                                    "Choose Grouping - date range:",
                                                    style={"font-size": "20px"},
                                                ),
                                                dcc.Dropdown(
                                                    id="grouping-dropdown3",
                                                    options=[
                                                        {
                                                            "label": "Full Spotify History",
                                                            "value": "full",
                                                        },
                                                        {
                                                            "label": "This semester",
                                                            "value": "sem",
                                                        }
                                                    ],
                                                    value="full",
                                                    style={
                                                        "font-size": "20px",
                                                    },
                                                ),
                                            ],
                                            style={
                                                "width": "30%",
                                                "float": "right",
                                            },
                                        ),
                                        html.Div(
                                            [
                                                html.Label(
                                                    "Choose Grouping - time frame:",
                                                    style={"font-size": "20px"},
                                                ),
                                                dcc.Dropdown(
                                                    id="grouping-dropdown2",
                                                    options=[
                                                        {
                                                            "label": "days",
                                                            "value": "Date",
                                                        },
                                                        {
                                                            "label": "day of the week",
                                                            "value": "day-name",
                                                        },
                                                        {
                                                            "label": "week number",
                                                            "value": "week_number",
                                                        },
                                                        {
                                                            "label": "year",
                                                            "value": "year",
                                                        },
                                                        {
                                                            "label": "month",
                                                            "value": "month",
                                                        }

                                                    ],
                                                    value="Date",
                                                    style={
                                                        "font-size": "20px",
                                                    },
                                                ),
                                            ],
                                            style={
                                                "width": "30%",
                                                "float": "right",
                                            },
                                        ),
                                    ],
                                    style={"display": "flex"},
                                ),
                                html.Div(
                                    dcc.Graph(id="ListeningTimeVsDate"),
                                    style={
                                        "width": "97%",
                                        "box-shadow": "2px 2px 2px 2px rgba(0.3, 0.3, 0.3, 0.3)",
                                        "margin": "auto",
                                        "background-color": "#E8DAC5",
                                    },
                                ),

                            ]
                        ),
                        html.Div(
                            [
                                html.H2(
                                    "The average amount of time spent on Spotify during different time periods",
                                    style={"text-align": "center"},
                                ),
                                html.Div(
                                    [
                                        html.Div(
                                            [
                                                html.Label(
                                                    "Choose included data:",
                                                    style={"font-size": "20px"},
                                                ),
                                                dcc.Checklist(
                                                    id="checkboxes2",
                                                    options=[
                                                        {
                                                            "label": "Include days without Spotify use",
                                                            "value": "all",
                                                        }
                                                    ],
                                                    value=["all"],
                                                    style={"font-size": "20px"},
                                                ),
                                            ],
                                            style={
                                                "width": "20%",
                                                "float": "left",
                                                "margin": "1.5%",
                                            },
                                        ),
                                        html.Div(
                                            [
                                                html.Label(
                                                    "Choose Grouping - time frame:",
                                                    style={"font-size": "20px"},
                                                ),
                                                dcc.Dropdown(
                                                    id="grouping-dropdown4",
                                                    options=[
                                                        {
                                                            "label": "day of the week",
                                                            "value": "day-name",
                                                        },
                                                        {
                                                            "label": "week number",
                                                            "value": "week_number",
                                                        },
                                                        {
                                                            "label": "year",
                                                            "value": "year",
                                                        },
                                                        {
                                                            "label": "month",
                                                            "value": "month",
                                                        }

                                                    ],
                                                    value="day-name",
                                                    style={
                                                        "font-size": "20px",
                                                    },
                                                ),
                                            ],
                                            style={
                                                "width": "30%",
                                                "float": "right",
                                            },
                                        ),
                                        html.Div(
                                            [
                                                html.Label(
                                                    "Choose Grouping - date range:",
                                                    style={"font-size": "20px"},
                                                ),
                                                dcc.Dropdown(
                                                    id="grouping-dropdown5",
                                                    options=[
                                                        {
                                                            "label": "Full Spotify History",
                                                            "value": "full",
                                                        },
                                                        {
                                                            "label": "This semester",
                                                            "value": "sem",
                                                        }
                                                    ],
                                                    value="full",
                                                    style={
                                                        "font-size": "20px",
                                                    },
                                                ),
                                            ],
                                            style={
                                                "width": "30%",
                                                "float": "right",
                                            },
                                        ),
                                    ],
                                    style={"display": "flex"},
                                ),

                                html.Div(
                                    dcc.Graph(id="MeanListeningTimeVsTime"),
                                    style={
                                        "width": "97%",
                                        "box-shadow": "2px 2px 2px 2px rgba(0.3, 0.3, 0.3, 0.3)",
                                        "margin": "auto",
                                        "background-color": "#E8DAC5",
                                    },
                                ),

                            ]
                        ),
                        html.Div(
                            [
                                html.H1(
                                    "The average amount of time spent on Spotify vs different time periods",
                                    style={"text-align": "center"},
                                ),
                                html.Div(
                                    [
                                        html.Div(
                                            [
                                                html.Label(
                                                    "Choose included data:",
                                                    style={"font-size": "20px"},
                                                ),
                                                dcc.Checklist(
                                                    id="checkboxes3",
                                                    options=[
                                                        {
                                                            "label": "Include days without Spotify use",
                                                            "value": "all",
                                                        }
                                                    ],
                                                    value=["all"],
                                                    style={"font-size": "20px"},
                                                ),
                                            ],
                                            style={
                                                "width": "20%",
                                                "float": "left",
                                                "margin": "1.5%",
                                            },
                                        ),
                                        html.Div(
                                            [
                                                html.Label(
                                                    "Choose Grouping - date range:",
                                                    style={"font-size": "20px"},
                                                ),
                                                dcc.Dropdown(
                                                    id="grouping-dropdown6",
                                                    options=[
                                                        {
                                                            "label": "Full Spotify History",
                                                            "value": "full",
                                                        },
                                                        {
                                                            "label": "This semester",
                                                            "value": "sem",
                                                        }
                                                    ],
                                                    value="full",
                                                    style={
                                                        "font-size": "20px",
                                                    },
                                                ),
                                            ],
                                            style={
                                                "width": "30%",
                                                "float": "right",
                                            },
                                        ),
                                        html.Div(
                                            [
                                                html.Label(
                                                    "Choose Grouping - time frame:",
                                                    style={"font-size": "20px"},
                                                ),
                                                dcc.Dropdown(
                                                    id="grouping-dropdown7",
                                                    options=[
                                                        {
                                                            "label": "day of the week",
                                                            "value": "day-name",
                                                        },
                                                        {
                                                            "label": "season",
                                                            "value": "season",
                                                        },
                                                        {
                                                            "label": "year",
                                                            "value": "year",
                                                        },
                                                        {
                                                            "label": "month",
                                                            "value": "month",
                                                        }

                                                    ],
                                                    value="year",
                                                    style={
                                                        "font-size": "20px",
                                                    },
                                                ),
                                            ],
                                            style={
                                                "width": "30%",
                                                "float": "right",
                                            },
                                        ),
                                    ],
                                    style={"display": "flex"},
                                ),
                                html.Div(
                                    dcc.Graph(id="ListeningTimeVsDateBoxPlot"),
                                    style={
                                        "width": "97%",
                                        "box-shadow": "2px 2px 2px 2px rgba(0.3, 0.3, 0.3, 0.3)",
                                        "margin": "auto",
                                        "background-color": "#E8DAC5",
                                    },
                                ),
                            ]
                        ),
                        html.Div(
                            style={
                                "height": "100px",
                                "background-color": "#FA8128",
                                "margin-top": "20px",
                            }
                        ),
                    ], style={"background-color": "#FA8128"},
                ),
                dcc.Tab(
                    label="Music Listening VS Our Calendar",
                    children=[
                        html.Div(
                            [

                            ],
                            id='div_4_content'
                        ),
                        html.Div(
                            [
                                html.H1(
                                    "Music Listening VS Our Calendar",
                                    style={"text-align": "center"},
                                ),
                                html.Div(
                                    [
                                        html.Div(
                                            [
                                                html.Label(
                                                    "Choose the variable for axis X:",
                                                    style={"font-size": "20px"},
                                                ),
                                                dcc.Dropdown(
                                                    id="x-axis-dropdown",
                                                    options=[
                                                        {
                                                            "label": "Total Academic Activities",
                                                            "value": "TotalAcademicActivities",
                                                        },
                                                        {
                                                            "label": "Coffee Cups",
                                                            "value": "CoffeeCups",
                                                        },
                                                        {
                                                            "label": "Average Energy Level",
                                                            "value": "AverageEnergyLevel",
                                                        },
                                                        {
                                                            "label": "Productivity",
                                                            "value": "Productivity",
                                                        },
                                                        {
                                                            "label": "Steps Count",
                                                            "value": "StepsCount",
                                                        },
                                                        {
                                                            "label": "Meeting",
                                                            "value": "Meeting",
                                                        },
                                                        {
                                                            "label": "Number Of Lectures Today",
                                                            "value": "NumberOfLecturesToday",
                                                        },
                                                        {
                                                            "label": "Working Hours",
                                                            "value": "WorkingHours",
                                                        },
                                                        {
                                                            "label": "Sleep Minutes",
                                                            "value": "SleepMinutes"
                                                        }
                                                    ],
                                                    value="SleepMinutes",
                                                    style={
                                                        "font-size": "20px",
                                                    },
                                                ),
                                            ],
                                            style={
                                                "width": "30%",
                                                "float": "right",
                                            },
                                        ),
                                        html.Div(
                                            [
                                                html.Label(
                                                    "Choose the other variable:",
                                                    style={"font-size": "20px"}
                                                ),
                                                dcc.Dropdown(
                                                    id="color-dropdown",
                                                    options=[
                                                        {
                                                            "label": "Total Academic Activities",
                                                            "value": "TotalAcademicActivities",
                                                        },
                                                        {
                                                            "label": "Coffee Cups",
                                                            "value": "CoffeeCups",
                                                        },
                                                        {
                                                            "label": "Average Energy Level",
                                                            "value": "AverageEnergyLevel",
                                                        },
                                                        {
                                                            "label": "Productivity",
                                                            "value": "Productivity",
                                                        },
                                                        {
                                                            "label": "Steps Count",
                                                            "value": "StepsCount",
                                                        },
                                                        {
                                                            "label": "Meeting",
                                                            "value": "Meeting",
                                                        },
                                                        {
                                                            "label": "Number Of Lectures Today",
                                                            "value": "NumberOfLecturesToday",
                                                        },
                                                        {
                                                            "label": "Working Hours",
                                                            "value": "WorkingHours",
                                                        },
                                                        {
                                                            "label": "Sleep Minutes",
                                                            "value": "SleepMinutes"
                                                        }
                                                    ],
                                                    value="NumberOfLecturesToday",
                                                    style={
                                                        "font-size": "20px",
                                                    },
                                                ),
                                            ],
                                            style={
                                                "width": "30%",
                                                "float": "right"
                                            },
                                        ),
                                    ],
                                    style={"display": "flex"},
                                ),
                                html.Div(
                                    dcc.Graph(id="scatter-plot"),
                                    style={
                                        "width": "97%",
                                        "margin": "auto",
                                        "background-color": "#E8DAC5",
                                    },
                                ),
                            ]
                        ),
                        html.Div(
                            style={
                                "height": "100px",
                                "background-color": "#FA8128",
                                "margin-top": "20px",
                            }
                        ),
                    ], style={"background-color": "#FA8128"},
                ),
                dcc.Tab([

                ], label="About Data", style={"background-color": "#FA8128"}),
            ]
        )
    ],
)


@app.callback(
    dash.dependencies.Output('div_1_content', 'children'),
    dash.dependencies.Output('div_2_content', 'children'),
    dash.dependencies.Output('div_3_content', 'children'),
    dash.dependencies.Output('div_4_content', 'children'),
    [
        dash.dependencies.Input('buttonBogusia', 'n_clicks'),
        dash.dependencies.Input('buttonWiktoria', 'n_clicks'),
        dash.dependencies.Input('buttonKasia', 'n_clicks')
    ]
)
def update_content(buttonBogusia_clicks, buttonWiktoria_clicks, buttonKasia_clicks):
    ctx = dash.callback_context
    global button_id
    button_id_1 = 'none'
    if ctx.triggered_id:
        button_id_1 = ctx.triggered_id.split('.')[0]
    if buttonBogusia_clicks == 0 and buttonWiktoria_clicks == 0 and buttonKasia_clicks == 0:
        button_id = 'buttonKasia'
    if button_id_1 == 'buttonBogusia':
        button_id = button_id_1
    elif button_id_1 == 'buttonWiktoria':
        button_id = button_id_1
    elif button_id_1 == 'buttonKasia':
        button_id = button_id_1

    if button_id == 'buttonBogusia':
        history = pd.read_csv("./Data/Bogusia/StreamingHistoryExtended.csv")
        history = history.dropna(subset=['ts'])
        history["Date"] = pd.to_datetime(history["ts"]).dt.date
        songs_info = pd.read_csv("Data/Common/songs_spotify_info.csv")
        songs_info = songs_info[["track_id", "track_genre"]]
        personalData = pd.read_csv("Data/Bogusia/personalDataFull.csv")
        all_data = pd.read_csv("Data/Bogusia/all_data.csv")  # data from DataPreprocessing
        personalData["Date"] = pd.to_datetime(personalData["Date"]).dt.date
        data_merged = pd.merge(history, personalData, on="Date")
        data_merged["spotify_track_uri"] = data_merged["spotify_track_uri"].str[14:]
    elif button_id == 'buttonWiktoria':
        history = pd.read_csv("./Data/Wiktoria/StreamingHistoryExtended.csv")
        history = history.dropna(subset=['ts'])
        history["Date"] = pd.to_datetime(history["ts"]).dt.date
        songs_info = pd.read_csv("Data/Common/songs_spotify_info.csv")
        songs_info = songs_info[["track_id", "track_genre"]]
        personalData = pd.read_csv("Data/Wiktoria/personalDataFull.csv")
        all_data = pd.read_csv("Data/Wiktoria/all_data.csv")  # data from DataPreprocessing
        personalData["Date"] = pd.to_datetime(personalData["Date"]).dt.date
        data_merged = pd.merge(history, personalData, on="Date")
        data_merged["spotify_track_uri"] = data_merged["spotify_track_uri"].str[14:]
    elif button_id == 'buttonKasia':
        history = pd.read_csv("./Data/Kasia/StreamingHistoryExtended.csv")
        history = history.dropna(subset=['ts'])
        history["Date"] = pd.to_datetime(history["ts"]).dt.date
        songs_info = pd.read_csv("Data/Common/songs_spotify_info.csv")
        songs_info = songs_info[["track_id", "track_genre"]]
        personalData = pd.read_csv("Data/Kasia/personalDataFull.csv")
        all_data = pd.read_csv("Data/Kasia/all_data.csv")  # data from DataPreprocessing
        personalData["Date"] = pd.to_datetime(personalData["Date"]).dt.date
        data_merged = pd.merge(history, personalData, on="Date")
        data_merged["spotify_track_uri"] = data_merged["spotify_track_uri"].str[14:]

    image_urls = \
    all_data.groupby(['master_metadata_album_artist_name', 'artists_photo'])['ms_played'].sum().sort_values(
        ascending=False).head(10).reset_index()['artists_photo']
    links = all_data.groupby(['master_metadata_album_artist_name', 'artist_on_spotify'])['ms_played'].sum().sort_values(
        ascending=False).head(10).reset_index()['artist_on_spotify']
    artists_names = \
    all_data.groupby(['master_metadata_album_artist_name'])['ms_played'].sum().sort_values(ascending=False).head(
        10).reset_index()['master_metadata_album_artist_name']
    image_albums_urls = \
    all_data.groupby(['master_metadata_album_album_name', 'album_cover'])['ms_played'].sum().sort_values(
        ascending=False).head(10).reset_index()['album_cover']
    links_albums = \
    all_data.groupby(['master_metadata_album_album_name', 'album_on_spotify'])['ms_played'].sum().sort_values(
        ascending=False).head(10).reset_index()['album_on_spotify']
    albums_names = all_data.groupby(['master_metadata_album_album_name', 'album_cover'])['ms_played'].sum().sort_values(
        ascending=False).head(10).reset_index()['master_metadata_album_album_name']

    #########wkładka 1 - zdjęcia artystów i albumów
    div_1_content = [
        html.Div([
            html.A(
                html.Div([
                    html.Img(src=image_url, style={"width": "95%", "margin": "2.5%"}),
                ]),
                href=link,
                target="_blank"
            ),
            html.Div([
                html.H6(artist_name, style={"text-align": "center"})
            ], style={'height': '50px'})
        ],
            style={"width": "20%", 'display': 'inline-block'})
        for image_url, link, artist_name in zip(image_urls, links, artists_names)

    ]

    div_2_content = [
        html.Div([
            html.A(
                html.Div([
                    html.Img(src=image_url, style={"width": "95%", "margin": "2.5%"}),
                ]),
                href=link,
                target="_blank"
            ),
            html.H6(artist_name, style={"text-align": "center"})
        ],
            style={"width": "20%", 'display': 'inline-block'})
        for image_url, link, artist_name in zip(image_albums_urls, links_albums, albums_names)
    ]

    category_order_reversed = ["High", "Medium", "Low"]
    category_order = ["Low", "Medium", "High"]
    #############rokład productivity
    grouped_data_productivity_distr_plot = (
        personalData.groupby(["Productivity"])["Date"].count().sort_values().reset_index()
    )
    productivity_distribution_plot = px.bar(
        grouped_data_productivity_distr_plot,
        x="Date",
        y="Productivity",
        title="Productivity distribution",
        labels={"Productivity": "Productivity", "Date": "Number of days in this semester"},
        orientation="h",
        color_discrete_sequence=["#FA8128"],
        category_orders={"Productivity": category_order_reversed},
    )
    productivity_distribution_plot.update_layout(
        xaxis_title="Number of days in this semester",
        yaxis_title="Productivity",
        font=dict(family="Georgia"),
        plot_bgcolor="rgba(0,0,0,0)",
        paper_bgcolor="rgba(0,0,0,0)",
        xaxis=dict(
            showgrid=True,
            gridcolor="white",
            gridwidth=2,
        ),
    )
    ##############violin-plot productivity plot
    grouped_data_median_ms_played_producctivity_plot = (
        data_merged.groupby(["Productivity", "Date"])["ms_played"].sum().reset_index()
    )
    grouped_data_median_ms_played_producctivity_plot["ms_played"] /= 60000
    boxplot_ms_played_producctivity_plot = px.violin(
        grouped_data_median_ms_played_producctivity_plot,
        x="Productivity",
        y="ms_played",
        box=True,
        category_orders={"Productivity": category_order},
    )
    boxplot_ms_played_producctivity_plot.update_layout(
        title="Plot of Productivity and Total Duration of listened music (Minutes) per Day",
        xaxis=dict(title="Productivity"),
        yaxis=dict(
            title="Total Duration of listened music (Minutes)",
            showgrid=True,
            gridcolor="white",
            gridwidth=2,
        ),
        font=dict(family="Georgia"),
        plot_bgcolor="rgba(0,0,0,0)",
        paper_bgcolor="rgba(0,0,0,0)",
    )
    boxplot_ms_played_producctivity_plot.update_traces(
        marker=dict(color="#1DB954"),
        spanmode='hard')

    div_3_content = [
        html.H1(
            "Productivity VS Music Preferences",
            style={"text-align": "center"},
        ),
        dcc.Graph(
            id="productivity-distribution",
            figure=productivity_distribution_plot,
            style={
                "width": "47%",
                "float": "left",
                "box-shadow": "2px 2px 2px 2px rgba(0.3, 0.3, 0.3, 0.3)",
                "margin-bottom": "1.5%",
                "background-color": "#E8DAC5",
                "margin-left": "1.5%",
            },
        ),
        dcc.Graph(
            id="median_ms_played-producctivity-plot",
            figure=boxplot_ms_played_producctivity_plot,
            style={
                "width": "47%",
                "float": "right",
                "box-shadow": "2px 2px 2px 2px rgba(0.3, 0.3, 0.3, 0.3)",
                "margin-bottom": "1.5%",
                "background-color": "#E8DAC5",
                "margin-right": "1.5%",
            },
        ),
    ]

    # Panele

    history_grouped = history.groupby('Date').agg({'ms_played': 'sum'})
    history_grouped['MusicMinutes'] = history_grouped['ms_played'] / 60000

    combined_data = pd.merge(personalData, history_grouped, how='left', on='Date')

    combined_data['MusicMinutes'] = combined_data['MusicMinutes'].fillna(0)

    combined_data['TotalAcademicActivities'] = combined_data['Projects'] + combined_data['Tests'] + combined_data[
        'NumberOfLecturesToday']

    combined_data['AverageEnergyLevel'] = (combined_data['EnergyLevelMorning'] + combined_data[
        'EnergyLevelEvening']) / 2

    average_coffee_cups = combined_data['CoffeeCups'].mean()

    total_steps_count = combined_data['StepsCount'].sum()

    shortest_sleep = combined_data['SleepMinutes'].min()

    longest_sleep = combined_data['SleepMinutes'].max()

    average_number_of_lectures = combined_data['NumberOfLecturesToday'].mean()

    total_meeting_count = combined_data['Meeting'].sum()

    combined_data['TotalTestsAndProjects'] = combined_data['Projects'] + combined_data['Tests']

    #    total_tests_and_projects = combined_data['TotalTestsAndProjects'].sum()

    div_4_content = [
        html.H1(
            "Our Calendar Data",
            style={"text-align": "center"},
        ),
        html.Div(
            [
                html.Div(
                    [
                        html.H3(f"{average_coffee_cups:.2f}",
                                style={"color": "#333", "fontSize": "2.5em", "margin": "0"}),
                        html.P("Average Number of Coffee Cups per Day",
                               style={"color": "#888", "fontSize": "1em", "marginTop": "0.25em"})
                    ],
                    style={
                        'margin': '2px',
                        'padding': '6px',
                        'height': '100px',
                        "backgroundColor": "#f9f9f9",
                        "borderLeft": "5px solid #333",
                        "boxShadow": "2px 2px 10px #aaa",
                        "width": "15%",
                        "borderRadius": "8px",
                        "textAlign": "center",
                        'justify-content': 'center',
                    },
                ),
                html.Div(
                    [
                        html.H3(f"{total_steps_count}",
                                style={"color": "#333", "fontSize": "2.5em", "margin": "0"}),
                        html.P("Total Steps Count", style={"color": "#888", "fontSize": "1em", "marginTop": "0.25em"
                                                           }),
                    ],
                    style={
                        'margin': '2px',
                        'padding': '6px',
                        'height': '100px',
                        "backgroundColor": "#f9f9f9",
                        "borderLeft": "5px solid #333",
                        "boxShadow": "2px 2px 10px #aaa",
                        "width": "15%",
                        "borderRadius": "8px",
                        "textAlign": "center",
                        'justify-content': 'center',
                    },
                ),
                html.Div(
                    [
                        html.H3(f"{shortest_sleep} min",
                                style={"color": "#333", "fontSize": "2.5em", "margin": "0",
                                       }),
                        html.P("Shortest Sleep Duration",
                               style={"color": "#888", "fontSize": "1em", "marginTop": "0.25em",
                                      }),
                    ],
                    style={
                        'margin': '2px',
                        'padding': '6px',
                        'justify-content': 'center',
                        'height': '100px',
                        "backgroundColor": "#f9f9f9",
                        "borderLeft": "5px solid #333",
                        "boxShadow": "2px 2px 10px #aaa",
                        "width": "15%",
                        "borderRadius": "8px",
                        "textAlign": "center"
                    },
                ),
                html.Div(
                    [
                        html.H3(f"{longest_sleep} min",
                                style={"color": "#333", "fontSize": "2.5em", "margin": "0",
                                       }),
                        html.P("Longest Sleep Duration",
                               style={"color": "#888", "fontSize": "1em", "marginTop": "0.25em",
                                      }),
                    ],
                    style={
                        'margin': '2px',
                        'padding': '6px',
                        'height': '100px',
                        "backgroundColor": "#f9f9f9",
                        "borderLeft": "5px solid #333",
                        "boxShadow": "2px 2px 10px #aaa",
                        "width": "15%",
                        "borderRadius": "8px",
                        "textAlign": "center",
                        'justify-content': 'center',
                    },
                ),
                html.Div(
                    [
                        html.H3(f"{average_number_of_lectures:.2f}",
                                style={"color": "#333", "fontSize": "2.5em", "margin": "0", }),
                        html.P("Average Number of Lectures per Day",
                               style={"color": "#888", "fontSize": "1em", "marginTop": "0.25em", }),
                    ],
                    style={
                        'margin': '2px',
                        'padding': '6px',
                        'height': '100px',
                        "backgroundColor": "#f9f9f9",
                        "borderLeft": "5px solid #333",
                        "boxShadow": "2px 2px 10px #aaa",
                        "width": "15%",
                        "borderRadius": "8px",
                        "textAlign": "center",
                        'justify-content': 'center',
                    },
                ),
                html.Div(
                    [
                        html.H3(f"{total_meeting_count}",
                                style={"color": "#333", "fontSize": "2.5em", "margin": "0"}),
                        html.P("Total Number of Meetings During this Semester",
                               style={"color": "#888", "fontSize": "1em", "marginTop": "0.25em"}),
                    ],
                    style={
                        'margin': '2px',
                        'padding': '6px',
                        'height': '100px',
                        "backgroundColor": "#f9f9f9",
                        "borderLeft": "5px solid #333",
                        "boxShadow": "2px 2px 10px #aaa",
                        "width": "15%",
                        "borderRadius": "8px",
                        "textAlign": "center",
                        'justify-content': 'center',
                    },
                ),
            ],
            style={"textAlign": "center", 'display': 'flex', 'justify-content': 'center', 'align-items': 'center'},
        ),
    ]

    return div_1_content, div_2_content, div_3_content, div_4_content


@app.callback(
    dash.dependencies.Output("genre_preference", "figure"),
    [
        dash.dependencies.Input("checkboxes1", "value"),
        dash.dependencies.Input("grouping-dropdown", "value"),
        dash.dependencies.Input('buttonBogusia', 'n_clicks'),
        dash.dependencies.Input('buttonWiktoria', 'n_clicks'),
        dash.dependencies.Input('buttonKasia', 'n_clicks')
    ],
)
def update_genre_preference(selected_productivity_values, selected_grouping_value, buttonBogusia_clicks,
                            buttonWiktoria_clicks, buttonKasia_clicks):
    global button_id
    if button_id == 'buttonBogusia':
        history = pd.read_csv("./Data/Bogusia/StreamingHistoryExtended.csv")
        history = history.dropna(subset=['ts'])
        history["Date"] = pd.to_datetime(history["ts"]).dt.date
        songs_info = pd.read_csv("Data/Common/songs_spotify_info.csv")
        songs_info = songs_info[["track_id", "track_genre"]]
        personalData = pd.read_csv("Data/Bogusia/personalDataFull.csv")
        all_data = pd.read_csv("Data/Bogusia/all_data.csv")  # data from DataPreprocessing
        personalData["Date"] = pd.to_datetime(personalData["Date"]).dt.date
    elif button_id == 'buttonWiktoria':
        history = pd.read_csv("./Data/Wiktoria/StreamingHistoryExtended.csv")
        history = history.dropna(subset=['ts'])
        history["Date"] = pd.to_datetime(history["ts"]).dt.date
        songs_info = pd.read_csv("Data/Common/songs_spotify_info.csv")
        songs_info = songs_info[["track_id", "track_genre"]]
        personalData = pd.read_csv("Data/Wiktoria/personalDataFull.csv")
        all_data = pd.read_csv("Data/Wiktoria/all_data.csv")  # data from DataPreprocessing
        personalData["Date"] = pd.to_datetime(personalData["Date"]).dt.date
    elif button_id == 'buttonKasia':
        history = pd.read_csv("./Data/Kasia/StreamingHistoryExtended.csv")
        history = history.dropna(subset=['ts'])
        history["Date"] = pd.to_datetime(history["ts"]).dt.date
        songs_info = pd.read_csv("Data/Common/songs_spotify_info.csv")
        songs_info = songs_info[["track_id", "track_genre"]]
        personalData = pd.read_csv("Data/Kasia/personalDataFull.csv")
        all_data = pd.read_csv("Data/Kasia/all_data.csv")  # data from DataPreprocessing
        personalData["Date"] = pd.to_datetime(personalData["Date"]).dt.date

    data_merged = pd.merge(history, personalData, on="Date")
    data_merged["spotify_track_uri"] = data_merged["spotify_track_uri"].str[14:]
    data = pd.merge(data_merged, songs_info, left_on="spotify_track_uri", right_on="track_id"
                    )

    if selected_grouping_value == "master_metadata_album_artist_name":
        grouping_columns = ["Productivity", selected_grouping_value]
        grouped_genre_preference_data = (
            data_merged.groupby(grouping_columns)["ms_played"]
            .sum()
            .sort_values(ascending=False)
            .reset_index()
        )
        filtered_data = grouped_genre_preference_data[
            grouped_genre_preference_data["Productivity"].isin(
                selected_productivity_values
            )
        ]
        filtered_data = (
            filtered_data.groupby(selected_grouping_value)["ms_played"]
            .sum()
            .sort_values()
            .reset_index()
            .tail(20)
        )
        title = "Top 20 Most Preferred Artists  Depending on Productivity"
        yaxis_label = "Artists"
    elif selected_grouping_value == "master_metadata_track_name":
        grouping_columns = ["Productivity", selected_grouping_value]
        grouped_genre_preference_data = (
            data_merged.groupby(grouping_columns)["ms_played"]
            .sum()
            .sort_values(ascending=False)
            .reset_index()
        )
        filtered_data = grouped_genre_preference_data[
            grouped_genre_preference_data["Productivity"].isin(
                selected_productivity_values
            )
        ]
        filtered_data = (
            filtered_data.groupby(selected_grouping_value)["ms_played"]
            .sum()
            .sort_values()
            .reset_index()
            .tail(20)
        )
        title = "Top 20 Most Preferred Songs  Depending on Productivity"
        yaxis_label = "Song"
    elif selected_grouping_value == "master_metadata_album_album_name":
        grouping_columns = ["Productivity", selected_grouping_value]
        grouped_genre_preference_data = (
            data_merged.groupby(grouping_columns)["ms_played"]
            .sum()
            .sort_values(ascending=False)
            .reset_index()
        )
        filtered_data = grouped_genre_preference_data[
            grouped_genre_preference_data["Productivity"].isin(
                selected_productivity_values
            )
        ]
        filtered_data = (
            filtered_data.groupby(selected_grouping_value)["ms_played"]
            .sum()
            .sort_values()
            .reset_index()
            .tail(20)
        )
        title = "Top 20 Most Preferred Albums Depending on Productivity"
        yaxis_label = "Album"
    else:
        grouping_columns = ["Productivity", selected_grouping_value]
        grouped_genre_preference_data = (
            data.groupby(grouping_columns)["ms_played"]
            .sum()
            .sort_values(ascending=False)
            .reset_index()
        )
        filtered_data = grouped_genre_preference_data[
            grouped_genre_preference_data["Productivity"].isin(
                selected_productivity_values
            )
        ]
        filtered_data = (
            filtered_data.groupby(selected_grouping_value)["ms_played"]
            .sum()
            .sort_values()
            .reset_index()
            .tail(20)
        )
        title = "Top 20 Most Preferred Genres of Music Depending on Productivity"
        yaxis_label = "Genre"

    filtered_data["ms_played"] /= 60000
    fig = px.bar(
        filtered_data, y=selected_grouping_value, x="ms_played", orientation="h"
    )
    fig.update_layout(
        title=title,
        xaxis=dict(title="Minutes listened", showgrid=True, gridcolor="white", gridwidth=2, ),
        yaxis=dict(title=yaxis_label),
        font=dict(family="Georgia"),
        plot_bgcolor="rgba(0,0,0,0)",
        paper_bgcolor="rgba(0,0,0,0)",
    )
    fig.update_traces(marker=dict(color="#1DB954"))
    return fig


@app.callback(
    dash.dependencies.Output("radar_plot", "figure"),
    [
        dash.dependencies.Input('buttonBogusia', 'n_clicks'),
        dash.dependencies.Input('buttonWiktoria', 'n_clicks'),
        dash.dependencies.Input('buttonKasia', 'n_clicks')
    ],
)
def update_radar_plot(buttonBogusia_clicks, buttonWiktoria_clicks, buttonKasia_clicks):
    ctx = dash.callback_context
    global button_id
    button_id_1 = 'none'
    if ctx.triggered_id:
        button_id_1 = ctx.triggered_id.split('.')[0]
    if buttonBogusia_clicks == 0 and buttonWiktoria_clicks == 0 and buttonKasia_clicks == 0:
        button_id = 'buttonKasia'
    if button_id_1 == 'buttonBogusia':
        button_id = button_id_1
    elif button_id_1 == 'buttonWiktoria':
        button_id = button_id_1
    elif button_id_1 == 'buttonKasia':
        button_id = button_id_1

    if button_id == 'buttonBogusia':
        history = pd.read_csv("./Data/Bogusia/StreamingHistoryExtended.csv")
        history = history.dropna(subset=['ts'])
        history["Date"] = pd.to_datetime(history["ts"]).dt.date
        songs_info = pd.read_csv("Data/Common/songs_spotify_info.csv")
        songs_info = songs_info[["track_id", "track_genre"]]
        personalData = pd.read_csv("Data/Bogusia/personalDataFull.csv")
        all_data = pd.read_csv("Data/Bogusia/all_data.csv")  # data from DataPreprocessing
        personalData["Date"] = pd.to_datetime(personalData["Date"]).dt.date
        data_merged = pd.merge(history, personalData, on="Date")
        data_merged["spotify_track_uri"] = data_merged["spotify_track_uri"].str[14:]
    elif button_id == 'buttonWiktoria':
        history = pd.read_csv("./Data/Wiktoria/StreamingHistoryExtended.csv")
        history = history.dropna(subset=['ts'])
        history["Date"] = pd.to_datetime(history["ts"]).dt.date
        songs_info = pd.read_csv("Data/Common/songs_spotify_info.csv")
        songs_info = songs_info[["track_id", "track_genre"]]
        personalData = pd.read_csv("Data/Wiktoria/personalDataFull.csv")
        all_data = pd.read_csv("Data/Wiktoria/all_data.csv")  # data from DataPreprocessing
        personalData["Date"] = pd.to_datetime(personalData["Date"]).dt.date
        data_merged = pd.merge(history, personalData, on="Date")
        data_merged["spotify_track_uri"] = data_merged["spotify_track_uri"].str[14:]
    elif button_id == 'buttonKasia':
        history = pd.read_csv("./Data/Kasia/StreamingHistoryExtended.csv")
        history = history.dropna(subset=['ts'])
        history["Date"] = pd.to_datetime(history["ts"]).dt.date
        songs_info = pd.read_csv("Data/Common/songs_spotify_info.csv")
        songs_info = songs_info[["track_id", "track_genre"]]
        personalData = pd.read_csv("Data/Kasia/personalDataFull.csv")
        all_data = pd.read_csv("Data/Kasia/all_data.csv")  # data from DataPreprocessing
        personalData["Date"] = pd.to_datetime(personalData["Date"]).dt.date
        data_merged = pd.merge(history, personalData, on="Date")
        data_merged["spotify_track_uri"] = data_merged["spotify_track_uri"].str[14:]

    grouped = all_data.groupby(['Productivity'])[
        ['acousticness', 'danceability', 'liveness', 'energy', 'valence', 'speechiness', 'loudness']].mean()
    categories = ['acousticness', 'danceability', 'liveness', 'energy', 'valence', 'speechiness', 'loudness']
    productivity_labels = grouped.index.tolist()
    data = grouped.values.tolist()
    radar_plot = go.Figure()

    colors = ['red', '#1E90FF', '#008000']

    for i in range(len(productivity_labels)):
        data[i].append(data[i][0])
        radar_plot.add_trace(go.Scatterpolar(
            r=data[i],
            theta=categories + [categories[0]],
            fill='toself',
            name=productivity_labels[i],
            line=dict(color=colors[i % len(colors)])
        ))

    radar_plot.update_layout(
        polar=dict(
            radialaxis=dict(
                visible=True,
                gridwidth=2,
                range=[0, 1],
            ),
        ),
        showlegend=True,
        plot_bgcolor="rgba(0,0,0,0)",
        paper_bgcolor="rgba(0,0,0,0)",
        title='Median values of music characteristics (scaled from 0 to 1) by productivity',
        font=dict(family='Georgia'),
        polar_bgcolor='rgba(255,255,255,0)'
    )
    return radar_plot


# Zakładka 3 - Date

# Funkcje

def czasWminutach(td):
    # To get the minutes information
    return (td / 1000) / 60


def uzupelnicDaty(data1):
    data2 = data1.copy()
    # start_date = pd.to_datetime(min(data2["Date"])).dt.date
    # end_date = pd.to_datetime(max(data2["Date"])).dt.date
    start_date = pd.to_datetime(min(data2["Date"]))
    end_date = pd.to_datetime(max(data2["Date"]))
    data2["Date"] = pd.to_datetime(data2["Date"]).dt.date

    a = pd.date_range(start_date, end_date).difference(data2["Date"])
    x = a.to_frame(name="Date").reset_index(drop=True)
    x['Date'] = pd.to_datetime(x['Date']).dt.date
    dataAllDates = pd.concat([data2, x]).reset_index(drop=True)
    dataAllDates["Date"] = pd.to_datetime(dataAllDates["Date"]).dt.date
    return dataAllDates


def dodacKolumnyCzasow(data1):
    data = data1.copy()
    data["Date"] = pd.to_datetime(data["Date"])
    data['week_number'] = data["Date"].dt.isocalendar().week
    data['day-name'] = data["Date"].apply(lambda xl: xl.day_name())
    data["Date"] = pd.to_datetime(data["Date"]).dt.date
    data['year'] = pd.DatetimeIndex(data["Date"]).year
    data['month'] = pd.DatetimeIndex(data["Date"]).month
    data['day'] = pd.DatetimeIndex(data["Date"]).day

    dataFilled = data.sort_values("Date")
    return dataFilled


@app.callback(
    dash.dependencies.Output("ListeningTimeVsDate", "figure"),
    [
        dash.dependencies.Input("grouping-dropdown2", "value"),
        dash.dependencies.Input("grouping-dropdown3", "value"),
        dash.dependencies.Input('buttonBogusia', 'n_clicks'),
        dash.dependencies.Input('buttonWiktoria', 'n_clicks'),
        dash.dependencies.Input('buttonKasia', 'n_clicks')
    ],
)
def update_ListeningTimeVsDate(selected_grouping_value, selected_date_range, buttonBogusia_clicks,
                               buttonWiktoria_clicks, buttonKasia_clicks):
    global button_id
    if button_id == 'buttonBogusia':
        history = pd.read_csv("./Data/Bogusia/StreamingHistoryExtended.csv")
        history = history.dropna(subset=['ts'])
        history["Date"] = pd.to_datetime(history["ts"]).dt.date
        songs_info = pd.read_csv("Data/Common/songs_spotify_info.csv")
        songs_info = songs_info[["track_id", "track_genre"]]
        personalData = pd.read_csv("Data/Bogusia/personalDataFull.csv")
        all_data = pd.read_csv("Data/Bogusia/all_data.csv")  # data from DataPreprocessing
        personalData["Date"] = pd.to_datetime(personalData["Date"]).dt.date
    elif button_id == 'buttonWiktoria':
        history = pd.read_csv("./Data/Wiktoria/StreamingHistoryExtended.csv")
        history = history.dropna(subset=['ts'])
        history["Date"] = pd.to_datetime(history["ts"]).dt.date
        songs_info = pd.read_csv("Data/Common/songs_spotify_info.csv")
        songs_info = songs_info[["track_id", "track_genre"]]
        personalData = pd.read_csv("Data/Wiktoria/personalDataFull.csv")
        all_data = pd.read_csv("Data/Wiktoria/all_data.csv")  # data from DataPreprocessing
        personalData["Date"] = pd.to_datetime(personalData["Date"]).dt.date
    elif button_id == 'buttonKasia':
        history = pd.read_csv("./Data/Kasia/StreamingHistoryExtended.csv")
        history = history.dropna(subset=['ts'])
        history["Date"] = pd.to_datetime(history["ts"]).dt.date
        songs_info = pd.read_csv("Data/Common/songs_spotify_info.csv")
        songs_info = songs_info[["track_id", "track_genre"]]
        personalData = pd.read_csv("Data/Kasia/personalDataFull.csv")
        all_data = pd.read_csv("Data/Kasia/all_data.csv")  # data from DataPreprocessing
        personalData["Date"] = pd.to_datetime(personalData["Date"]).dt.date

    data_merged = pd.merge(history, personalData, on="Date")
    data_merged["spotify_track_uri"] = data_merged["spotify_track_uri"].str[14:]
    data = pd.merge(data_merged, songs_info, left_on="spotify_track_uri", right_on="track_id"
                    ),
    # ### history_with_time_columns
    history_with_time_columns = dodacKolumnyCzasow(history)
    history_with_time_columns["Time(Minutes)"] = history_with_time_columns["ms_played"].apply(czasWminutach).round(5)
    history_with_time_columns = history_with_time_columns.sort_values("Date").reset_index(drop=True)
    # ### data_merged_with_time_columns
    data_merged_with_time_columns = dodacKolumnyCzasow(data_merged)
    data_merged_with_time_columns["Time(Minutes)"] = data_merged_with_time_columns["ms_played"].apply(
        czasWminutach).round(5)
    data_merged_with_time_columns = data_merged_with_time_columns.sort_values("Date").reset_index(drop=True)
    # ### historyAllDates
    historyAllDates = uzupelnicDaty(history_with_time_columns)
    historyAllDates = historyAllDates.sort_values("Date").reset_index(drop=True)
    # ### data_mergedAllDates
    data_mergedAllDates = uzupelnicDaty(data_merged_with_time_columns)
    data_mergedAllDates = data_mergedAllDates.sort_values("Date").reset_index(drop=True)
    day_order = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
    if selected_date_range == "full":
        data2 = historyAllDates
    else:
        data2 = data_mergedAllDates
    if selected_grouping_value == "Date":
        grouped_date_time_data = (
            data2.groupby(selected_grouping_value, as_index=False)["Time(Minutes)"]
            .sum()
            .sort_values(selected_grouping_value)
            .reset_index(drop=True)
        )
        title = "Time spent on Spotify per day"
        xaxis_label = "Date"
    elif selected_grouping_value == "day-name":
        data2['day-name'] = pd.Categorical(data2['day-name'], categories=day_order, ordered=True)
        grouped_date_time_data = (
            data2.groupby(selected_grouping_value, as_index=False)["Time(Minutes)"]
            .sum()
            .sort_values(selected_grouping_value)
            .reset_index(drop=True)
        )
        title = "Time spent on Spotify per day of the week"
        xaxis_label = "dayname"
    elif selected_grouping_value == "week_number":
        grouped_date_time_data = (
            data2.groupby(selected_grouping_value, as_index=False)["Time(Minutes)"]
            .sum()
            .sort_values(selected_grouping_value)
            .reset_index(drop=True)
        )
        title = "Time spent on Spotify per the week of the year"
        xaxis_label = "week number"
    elif selected_grouping_value == "month":
        grouped_date_time_data = (
            data2.groupby(selected_grouping_value, as_index=False)["Time(Minutes)"]
            .sum()
            .sort_values(selected_grouping_value)
            .reset_index(drop=True)
        )
        title = "Time spent on Spotify per the month of the year"
        xaxis_label = "month number"
    else:
        grouped_date_time_data = (
            data2.groupby(selected_grouping_value, as_index=False)["Time(Minutes)"]
            .sum()
            .sort_values(selected_grouping_value)
            .reset_index(drop=True)
        )
        title = "Time spent on Spotify per year"
        xaxis_label = "year"
    fig = px.line(
        grouped_date_time_data, x=selected_grouping_value, y="Time(Minutes)"
    )

    fig.update_layout(
        title=title,
        yaxis=dict(title="Minutes listened", showgrid=True, gridcolor="white", gridwidth=2, ),
        xaxis=dict(title=xaxis_label),
        font=dict(family="Georgia"),
        plot_bgcolor="rgba(0,0,0,0)",
        paper_bgcolor="rgba(0,0,0,0)",
    )
    fig.update_traces(line=dict(color="#1DB954"))
    return fig


@app.callback(
    dash.dependencies.Output("MeanListeningTimeVsTime", "figure"),
    [
        dash.dependencies.Input("checkboxes2", "value"),
        dash.dependencies.Input("grouping-dropdown4", "value"),
        dash.dependencies.Input("grouping-dropdown5", "value"),
        dash.dependencies.Input('buttonBogusia', 'n_clicks'),
        dash.dependencies.Input('buttonWiktoria', 'n_clicks'),
        dash.dependencies.Input('buttonKasia', 'n_clicks')
    ],
)
def update_MeanListeningTimeVsTime(selected_data_values, selected_grouping_value, selected_date_range,
                                   buttonBogusia_clicks, buttonWiktoria_clicks, buttonKasia_clicks):
    global button_id
    if button_id == 'buttonBogusia':
        history = pd.read_csv("./Data/Bogusia/StreamingHistoryExtended.csv")
        history = history.dropna(subset=['ts'])
        history["Date"] = pd.to_datetime(history["ts"]).dt.date
        songs_info = pd.read_csv("Data/Common/songs_spotify_info.csv")
        songs_info = songs_info[["track_id", "track_genre"]]
        personalData = pd.read_csv("Data/Bogusia/personalDataFull.csv")
        all_data = pd.read_csv("Data/Bogusia/all_data.csv")  # data from DataPreprocessing
        personalData["Date"] = pd.to_datetime(personalData["Date"]).dt.date
    elif button_id == 'buttonWiktoria':
        history = pd.read_csv("./Data/Wiktoria/StreamingHistoryExtended.csv")
        history = history.dropna(subset=['ts'])
        history["Date"] = pd.to_datetime(history["ts"]).dt.date
        songs_info = pd.read_csv("Data/Common/songs_spotify_info.csv")
        songs_info = songs_info[["track_id", "track_genre"]]
        personalData = pd.read_csv("Data/Wiktoria/personalDataFull.csv")
        all_data = pd.read_csv("Data/Wiktoria/all_data.csv")  # data from DataPreprocessing
        personalData["Date"] = pd.to_datetime(personalData["Date"]).dt.date
    elif button_id == 'buttonKasia':
        history = pd.read_csv("./Data/Kasia/StreamingHistoryExtended.csv")
        history = history.dropna(subset=['ts'])
        history["Date"] = pd.to_datetime(history["ts"]).dt.date
        songs_info = pd.read_csv("Data/Common/songs_spotify_info.csv")
        songs_info = songs_info[["track_id", "track_genre"]]
        personalData = pd.read_csv("Data/Kasia/personalDataFull.csv")
        all_data = pd.read_csv("Data/Kasia/all_data.csv")  # data from DataPreprocessing
        personalData["Date"] = pd.to_datetime(personalData["Date"]).dt.date

    data_merged = pd.merge(history, personalData, on="Date")
    data_merged["spotify_track_uri"] = data_merged["spotify_track_uri"].str[14:]
    data = pd.merge(data_merged, songs_info, left_on="spotify_track_uri", right_on="track_id"
                    ),
    # ### history_with_time_columns
    history_with_time_columns = dodacKolumnyCzasow(history)
    history_with_time_columns["Time(Minutes)"] = history_with_time_columns["ms_played"].apply(czasWminutach).round(5)
    history_with_time_columns = history_with_time_columns.sort_values("Date").reset_index(drop=True)
    # ### data_merged_with_time_columns
    data_merged_with_time_columns = dodacKolumnyCzasow(data_merged)
    data_merged_with_time_columns["Time(Minutes)"] = data_merged_with_time_columns["ms_played"].apply(
        czasWminutach).round(5)
    data_merged_with_time_columns = data_merged_with_time_columns.sort_values("Date").reset_index(drop=True)
    # ### historyAllDates
    historyAllDates = uzupelnicDaty(history_with_time_columns)
    historyAllDates = historyAllDates.sort_values("Date").reset_index(drop=True)
    # ### data_mergedAllDates
    data_mergedAllDates = uzupelnicDaty(data_merged_with_time_columns)
    data_mergedAllDates = data_mergedAllDates.sort_values("Date").reset_index(drop=True)

    if selected_data_values == ["all"]:
        if selected_date_range == "full":
            data2 = historyAllDates
        else:
            data2 = data_mergedAllDates
    else:
        if selected_date_range == "full":
            data2 = history_with_time_columns
        else:
            data2 = data_merged_with_time_columns

    if selected_grouping_value == "day-name":
        grouping_columns = ["Date", selected_grouping_value]
        grouped_date_time_data1 = (
            data2.groupby(grouping_columns, as_index=False)["Time(Minutes)"]
            .sum()
            .sort_values("Date")
            .reset_index()
        )
        grouped_date_time_data = (
            grouped_date_time_data1.groupby(selected_grouping_value, as_index=False)["Time(Minutes)"]
            .mean()
        )
        title = "The average amount of time spent on Spotify during different days of the week"
        xaxis_label = "Day of the week"

    elif selected_grouping_value == "week_number":
        grouping_columns = ["Date", selected_grouping_value]
        grouped_date_time_data1 = (
            data2.groupby(grouping_columns, as_index=False)["Time(Minutes)"]
            .sum()
            .sort_values("Date")
            .reset_index()
        )
        grouped_date_time_data = (
            grouped_date_time_data1.groupby(selected_grouping_value, as_index=False)["Time(Minutes)"]
            .mean()
        )
        title = "The average amount of time spent on Spotify during different days of the week"
        xaxis_label = "Week of the year"

    elif selected_grouping_value == "year":
        grouping_columns = ["Date", selected_grouping_value]
        grouped_date_time_data1 = (
            data2.groupby(grouping_columns, as_index=False)["Time(Minutes)"]
            .sum()
            .sort_values("Date")
            .reset_index()
        )
        grouped_date_time_data = (
            grouped_date_time_data1.groupby(selected_grouping_value, as_index=False)["Time(Minutes)"]
            .mean()
        )
        title = "The average amount of time spent on Spotify during different days of the week"
        xaxis_label = "Year"

    else:
        grouping_columns = ["Date", selected_grouping_value]
        grouped_date_time_data1 = (
            data2.groupby(grouping_columns, as_index=False)["Time(Minutes)"]
            .sum()
            .sort_values("Date")
            .reset_index()
        )
        grouped_date_time_data = (
            grouped_date_time_data1.groupby(selected_grouping_value, as_index=False)["Time(Minutes)"]
            .mean()
        )
        title = "The average amount of time spent on Spotify during different days of the week"
        xaxis_label = "Month of the year"

    if selected_grouping_value == "day-name":
        fig = px.bar(
            grouped_date_time_data, x='day-name', y='Time(Minutes)',
            category_orders={'day-name': ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']}
        )
        fig.update_layout(
            title=title,
            yaxis=dict(title="Minutes listened", showgrid=True, gridcolor="white", gridwidth=2, ),
            xaxis=dict(title=xaxis_label),
            font=dict(family="Georgia"),
            plot_bgcolor="rgba(0,0,0,0)",
            paper_bgcolor="rgba(0,0,0,0)",
        )
    else:
        fig = px.bar(
            grouped_date_time_data, x=selected_grouping_value, y='Time(Minutes)'
        )
        fig.update_layout(
            title=title,
            yaxis=dict(title="Minutes listened", showgrid=True, gridcolor="white", gridwidth=2, ),
            xaxis=dict(title=xaxis_label),
            font=dict(family="Georgia"),
            plot_bgcolor="rgba(0,0,0,0)",
            paper_bgcolor="rgba(0,0,0,0)",
        )
    fig.update_traces(marker=dict(color="#1DB954"))
    return fig


@app.callback(
    dash.dependencies.Output("ListeningTimeVsDateBoxPlot", "figure"),
    [
        dash.dependencies.Input("checkboxes3", "value"),
        dash.dependencies.Input("grouping-dropdown7", "value"),
        dash.dependencies.Input("grouping-dropdown6", "value"),
        dash.dependencies.Input('buttonBogusia', 'n_clicks'),
        dash.dependencies.Input('buttonWiktoria', 'n_clicks'),
        dash.dependencies.Input('buttonKasia', 'n_clicks')
    ],
)
def update_ListeningTimeVsDateBoxPlot(selected_data_values, selected_grouping_value, selected_date_range,
                                      buttonBogusia_clicks, buttonWiktoria_clicks, buttonKasia_clicks):
    global button_id
    if button_id == 'buttonBogusia':
        history = pd.read_csv("./Data/Bogusia/StreamingHistoryExtended.csv")
        history = history.dropna(subset=['ts'])
        history["Date"] = pd.to_datetime(history["ts"]).dt.date
        songs_info = pd.read_csv("Data/Common/songs_spotify_info.csv")
        songs_info = songs_info[["track_id", "track_genre"]]
        personalData = pd.read_csv("Data/Bogusia/personalDataFull.csv")
        all_data = pd.read_csv("Data/Bogusia/all_data.csv")  # data from DataPreprocessing
        personalData["Date"] = pd.to_datetime(personalData["Date"]).dt.date
    elif button_id == 'buttonWiktoria':
        history = pd.read_csv("./Data/Wiktoria/StreamingHistoryExtended.csv")
        history = history.dropna(subset=['ts'])
        history["Date"] = pd.to_datetime(history["ts"]).dt.date
        songs_info = pd.read_csv("Data/Common/songs_spotify_info.csv")
        songs_info = songs_info[["track_id", "track_genre"]]
        personalData = pd.read_csv("Data/Wiktoria/personalDataFull.csv")
        all_data = pd.read_csv("Data/Wiktoria/all_data.csv")  # data from DataPreprocessing
        personalData["Date"] = pd.to_datetime(personalData["Date"]).dt.date
    elif button_id == 'buttonKasia':
        history = pd.read_csv("./Data/Kasia/StreamingHistoryExtended.csv")
        history = history.dropna(subset=['ts'])
        history["Date"] = pd.to_datetime(history["ts"]).dt.date
        songs_info = pd.read_csv("Data/Common/songs_spotify_info.csv")
        songs_info = songs_info[["track_id", "track_genre"]]
        personalData = pd.read_csv("Data/Kasia/personalDataFull.csv")
        all_data = pd.read_csv("Data/Kasia/all_data.csv")  # data from DataPreprocessing
        personalData["Date"] = pd.to_datetime(personalData["Date"]).dt.date

    data_merged = pd.merge(history, personalData, on="Date")
    data_merged["spotify_track_uri"] = data_merged["spotify_track_uri"].str[14:]
    data = pd.merge(data_merged, songs_info, left_on="spotify_track_uri", right_on="track_id"
                    ),
    # ### history_with_time_columns
    history_with_time_columns = dodacKolumnyCzasow(history)
    history_with_time_columns["Time(Minutes)"] = history_with_time_columns["ms_played"].apply(czasWminutach).round(5)
    history_with_time_columns = history_with_time_columns.sort_values("Date").reset_index(drop=True)
    # ### data_merged_with_time_columns
    data_merged_with_time_columns = dodacKolumnyCzasow(data_merged)
    data_merged_with_time_columns["Time(Minutes)"] = data_merged_with_time_columns["ms_played"].apply(
        czasWminutach).round(5)
    data_merged_with_time_columns = data_merged_with_time_columns.sort_values("Date").reset_index(drop=True)
    # ### historyAllDates
    historyAllDates = uzupelnicDaty(history_with_time_columns)
    historyAllDates = historyAllDates.sort_values("Date").reset_index(drop=True)
    # ### data_mergedAllDates
    data_mergedAllDates = uzupelnicDaty(data_merged_with_time_columns)
    data_mergedAllDates = data_mergedAllDates.sort_values("Date").reset_index(drop=True)

    if selected_data_values == ["all"]:
        if selected_date_range == "full":
            data2 = historyAllDates
        else:
            data2 = data_mergedAllDates
    else:
        if selected_date_range == "full":
            data2 = history_with_time_columns
        else:
            data2 = data_merged_with_time_columns

    if selected_grouping_value == "day-name":
        grouping_columns = ["Date", selected_grouping_value]
        grouped_date_time_data1 = (
            data2.groupby(grouping_columns, as_index=False)["Time(Minutes)"]
            .sum()
            .sort_values("Date")
            .reset_index()
        )
        grouped_date_time_data = (
            grouped_date_time_data1.groupby(selected_grouping_value, as_index=False)["Time(Minutes)"]
            .mean()
        )
        title = "The average amount of time spent on Spotify during different days of the week"
        xaxis_label = "Day of the week"

    elif selected_grouping_value == "season":
        data2['season'] = data2['Date'].apply(get_season)
        grouping_columns = ["Date", selected_grouping_value]
        grouped_date_time_data1 = (
            data2.groupby(grouping_columns, as_index=False)["Time(Minutes)"]
            .sum()
            .sort_values("Date")
            .reset_index()
        )
        grouped_date_time_data = (
            grouped_date_time_data1.groupby(selected_grouping_value, as_index=False)["Time(Minutes)"]
            .mean()
        )
        title = "The average amount of time spent on Spotify during different seasons"
        xaxis_label = "Season of the year"

    elif selected_grouping_value == "year":
        grouping_columns = ["Date", selected_grouping_value]
        grouped_date_time_data1 = (
            data2.groupby(grouping_columns, as_index=False)["Time(Minutes)"]
            .sum()
            .sort_values("Date")
            .reset_index()
        )
        grouped_date_time_data = (
            grouped_date_time_data1.groupby(selected_grouping_value, as_index=False)["Time(Minutes)"]
            .mean()
        )
        title = "The average amount of time spent on Spotify during different days of the week"
        xaxis_label = "Year"

    else:
        grouping_columns = ["Date", selected_grouping_value]
        grouped_date_time_data1 = (
            data2.groupby(grouping_columns, as_index=False)["Time(Minutes)"]
            .sum()
            .sort_values("Date")
            .reset_index()
        )
        grouped_date_time_data = (
            grouped_date_time_data1.groupby(selected_grouping_value, as_index=False)["Time(Minutes)"]
            .mean()
        )
        title = "The average amount of time spent on Spotify during different days of the week"
        xaxis_label = "Month of the year"

    if selected_grouping_value == "season":
        color_dict = {'Spring': 'green', 'Summer': 'orange', 'Fall': 'red', 'Winter': 'blue'}
        fig = px.box(grouped_date_time_data1, x='season', y='Time(Minutes)',
                     category_orders={'season': ['Spring', 'Summer', 'Fall', 'Winter']},
                     color='season', color_discrete_map=color_dict)
        fig.update_layout(
            title=title,
            yaxis=dict(title="Minutes listened", showgrid=True, gridcolor="white", gridwidth=2, ),
            xaxis=dict(title=xaxis_label),
            font=dict(family="Georgia"),
            plot_bgcolor="rgba(0,0,0,0)",
            paper_bgcolor="rgba(0,0,0,0)",
        )
    elif selected_grouping_value == "day-name":
        # color_dict = {'Spring': 'green', 'Summer': 'orange', 'Fall': 'red', 'Winter': 'blue'}
        fig = px.box(grouped_date_time_data1, x='day-name', y='Time(Minutes)',
                     category_orders={
                         'day-name': ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']})
        fig.update_layout(
            title=title,
            yaxis=dict(title="Minutes listened", showgrid=True, gridcolor="white", gridwidth=2, ),
            xaxis=dict(title=xaxis_label),
            font=dict(family="Georgia"),
            plot_bgcolor="rgba(0,0,0,0)",
            paper_bgcolor="rgba(0,0,0,0)",
        )
    else:
        fig = px.box(
            grouped_date_time_data1, x=selected_grouping_value, y="Time(Minutes)"
        )
        fig.update_layout(
            title=title,
            yaxis=dict(title="Minutes listened", showgrid=True, gridcolor="white", gridwidth=2, ),
            xaxis=dict(title=xaxis_label),
            font=dict(family="Georgia"),
            plot_bgcolor="rgba(0,0,0,0)",
            paper_bgcolor="rgba(0,0,0,0)",
        )
        fig.update_traces(marker=dict(color="#1DB954"))
    return fig


# Scatter plot

@app.callback(
    dash.dependencies.Output("scatter-plot", "figure"),
    [
        dash.dependencies.Input("x-axis-dropdown", "value"),
        dash.dependencies.Input("color-dropdown", "value"),
        dash.dependencies.Input('buttonBogusia', 'n_clicks'),
        dash.dependencies.Input('buttonWiktoria', 'n_clicks'),
        dash.dependencies.Input('buttonKasia', 'n_clicks')
    ],
)
def update_scatter_plot(x_axis_value, color_value, buttonBogusia_clicks, buttonWiktoria_clicks, buttonKasia_clicks):
    global button_id
    label_dict = {
        "TotalAcademicActivities": "Total Academic Activities",
        "CoffeeCups": "Coffee Cups",
        "AverageEnergyLevel": "Average Energy Level",
        "StepsCount": "Steps Count",
        "NumberOfLecturesToday": "Number Of Lectures Today",
        "WorkingHours": "Working Hours",
        "SleepMinutes": "Sleep Minutes",
    }
    if button_id == 'buttonBogusia':
        history = pd.read_csv("./Data/Bogusia/StreamingHistoryExtended.csv")
        history = history.dropna(subset=['ts'])
        history["Date"] = pd.to_datetime(history["ts"]).dt.date
        songs_info = pd.read_csv("Data/Common/songs_spotify_info.csv")
        songs_info = songs_info[["track_id", "track_genre"]]
        personalData = pd.read_csv("Data/Bogusia/personalDataFull.csv")
        all_data = pd.read_csv("Data/Bogusia/all_data.csv")  # data from DataPreprocessing
        personalData["Date"] = pd.to_datetime(personalData["Date"]).dt.date
    elif button_id == 'buttonWiktoria':
        history = pd.read_csv("./Data/Wiktoria/StreamingHistoryExtended.csv")
        history = history.dropna(subset=['ts'])
        history["Date"] = pd.to_datetime(history["ts"]).dt.date
        songs_info = pd.read_csv("Data/Common/songs_spotify_info.csv")
        songs_info = songs_info[["track_id", "track_genre"]]
        personalData = pd.read_csv("Data/Wiktoria/personalDataFull.csv")
        all_data = pd.read_csv("Data/Wiktoria/all_data.csv")  # data from DataPreprocessing
        personalData["Date"] = pd.to_datetime(personalData["Date"]).dt.date
    elif button_id == 'buttonKasia':
        history = pd.read_csv("./Data/Kasia/StreamingHistoryExtended.csv")
        history = history.dropna(subset=['ts'])
        history["Date"] = pd.to_datetime(history["ts"]).dt.date
        songs_info = pd.read_csv("Data/Common/songs_spotify_info.csv")
        songs_info = songs_info[["track_id", "track_genre"]]
        personalData = pd.read_csv("Data/Kasia/personalDataFull.csv")
        all_data = pd.read_csv("Data/Kasia/all_data.csv")  # data from DataPreprocessing
        personalData["Date"] = pd.to_datetime(personalData["Date"]).dt.date

    history_grouped = history.groupby('Date').agg({'ms_played': 'sum'})
    history_grouped['MusicMinutes'] = history_grouped['ms_played'] / 60000
    combined_data = pd.merge(personalData, history_grouped, how='left', on='Date')
    combined_data['MusicMinutes'] = combined_data['MusicMinutes'].fillna(0)
    combined_data['TotalAcademicActivities'] = combined_data['Projects'] + combined_data['Tests'] + combined_data[
        'NumberOfLecturesToday']
    combined_data['AverageEnergyLevel'] = (combined_data['EnergyLevelMorning'] + combined_data[
        'EnergyLevelEvening']) / 2
    data_merged = pd.merge(history, personalData, on="Date")
    data_merged["spotify_track_uri"] = data_merged["spotify_track_uri"].str[14:]
    data = pd.merge(data_merged, songs_info, left_on="spotify_track_uri", right_on="track_id"
                    )

    xaxis_label = label_dict.get(x_axis_value, x_axis_value)
    color_label = label_dict.get(color_value, color_value)

    x_axis_for_plot = label_dict.get(x_axis_value, x_axis_value)
    color_for_plot = label_dict.get(color_value, color_value)

    modified_data = combined_data.copy()

    for original_name, new_name in label_dict.items():
        if original_name in modified_data.columns:
            modified_data = modified_data.rename(columns={original_name: new_name})

    fig = px.scatter(
        modified_data,
        x=x_axis_for_plot,
        y='MusicMinutes',
        color=color_for_plot
    )
    fig.update_layout(
        title="Music Listening Time vs. Our Calendar Data",
        xaxis=dict(title=xaxis_label, showgrid=True, gridcolor="white", gridwidth=2, ),
        yaxis=dict(title="Music Listening Time (Minutes)"),
        font=dict(family="Georgia"),
        legend=dict(title=color_label),
        plot_bgcolor="rgba(0,0,0,0)",
        paper_bgcolor="rgba(0,0,0,0)",
    )
    return fig


if __name__ == '__main__':
    app.run_server(debug=True)